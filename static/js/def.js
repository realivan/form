loaded_files = [];
gender = '';
is_send = false;
men = 'Мужской';
women = 'Женский';

// функция определения высоты блока с чатом
function chat_height(){
    var div = document.getElementById('form_block');
    var chat = document.getElementById('chat_block');
    chat.style.height = div.clientHeight+'px';
    var message_block = document.getElementById('message_block');
    message_block.style.height = chat.clientHeight-110+'px';
    if(!is_send){
        send_message('robot');
        is_send = true;
    }
    detect_border();
}

function men_gender() {
    gender = men;
    var table = document.getElementById('main_table');
    var num_rows = table.rows.length;
    num_rows = del_row(table, num_rows);
    table.insertRow(num_rows);
    table.rows[num_rows].insertCell(0); // добавляем ячейки
    table.rows[num_rows].insertCell(1);
    table.rows[num_rows].insertCell(2);
    table.rows[num_rows].cells[0].innerHTML =
	      '<label for="tel">Номер телефона своей девушки:</label>';
    table.rows[num_rows].cells[1].innerHTML =
	      '<input type="text" name="tel" id="tel" onchange="inter(this.value)" required placeholder="+375 29 000-00-00" />';
    table.rows[num_rows].cells[2].innerHTML =
	      '<span class="third_column">Только Беларусь</span>';
    table.rows[num_rows].cells[2].setAttribute('class', 'third_column');
    table.insertRow(num_rows+1);
    table.rows[num_rows+1].insertCell(0); // добавляем ячейки
    table.rows[num_rows+1].insertCell(1);
    table.rows[num_rows+1].cells[0].innerHTML =
	      'За какую футбольную команду болеешь:';
    table.rows[num_rows+1].cells[1].innerHTML =
	      '<label><input list="team" name="team" required></label>' +
          '<datalist id="team">' +
              '<option value="ФК Гомель">' +
              '<option value="БАТЭ">' +
              '<option value="Динамо Минск">' +
              '<option value="Торпедо-БЕЛАЗ">' +
              '<option value="Шахтер">' +
              '<option value="Крумкачы">' +
              '<option value="Ислочь">' +
              '<option value="Гранит">' +
              '<option value="Днепр Рогачев">' +
          '</datalist>';
    chat_height();      
}
function women_gender() {
    gender = 'Женский';
    var table = document.getElementById('main_table');
    var num_rows = table.rows.length;
    num_rows = del_row(table, num_rows);
    table.insertRow(num_rows);
    table.rows[num_rows].insertCell(0); // добавляем ячейки
    table.rows[num_rows].insertCell(1);
    table.rows[num_rows].cells[0].innerHTML =
	      '<label for="tel2">Номер телефона своего парня:</label>';
    table.rows[num_rows].cells[1].innerHTML =
	      '<input type="text" name="tel2" id="tel2" onchange="inter(this.value)" placeholder="+375 29 000-00-00" required />';
    chat_height();      
}
// функция определяет белорусские или международные номера
function inter(tel){
    var telefone = tel.split(' ');
    var sk = document.getElementById('sk');
    if(telefone[0] != '+375'){
        if(gender == women || decodeURIComponent(get_cookie('Пол')) == women){
            if(!document.getElementById('edit_hide_div')) {
                sk.setAttribute('value', 'Tefal');
                sk.disabled = true;
            }
        }else{
            return false;
        }
    }else{
        sk.disabled = false;
    }
    return true;
}
// функция замены строк в таблице в зависимлсти от пола
function del_row(table, num_rows) {
    if (num_rows == 14) {
        table.deleteRow(num_rows - 2);
        table.deleteRow(num_rows - 3);
        return num_rows - 3;
    }else{
        table.deleteRow(num_rows - 2);
        return num_rows - 2;
    }
}
// функция имитация перехода от шага к шагу
function show_hide(element_id, plus_minus, form) {
    var index = 2;
    if(element_id == 'first_step'){
        index = 0;
        if(!validate_first(form)){
            return;
        }
    }else if(element_id == 'second_step') {
        index = 1;
        if (!validate_second(form)) {
            return;
        }
    }
    //Если элемент с id-шником element_id существует
    var l = ['first_step', 'second_step', 'third_step'];
    if (document.getElementById(element_id)) {
        for (var i = 0; i < l.length; i++) {
            var obj = document.getElementById(l[i]);
            var table = document.getElementById('table_'+l[i]);
            if (obj.style.display != "block" && i == index + plus_minus) {
                obj.style.display = "block"; //Показываем элемент
                table.style.borderBottom = 'solid #19cdac';
                table.style.color = '#19cbac';
            }else{
                obj.style.display = "none";//Скрываем элемент
                table.style.borderBottom = '1px solid #f0f0f0';
                table.style.color = 'black';
            }
            if(l[i] == 'third_step'){
                show_data('own_data');
            }
        }
        chat_height();
    }
}
// функция показывает введенные пользователем данные
function show_data(element){
    var i = 0;
    var j = 6;
    var table = document.getElementById(element);
    var elements = table.getElementsByTagName('p');
    var mas = document.cookie.split(';'), names = [];
    if(element == 'card_data'){
        i = mas.length-4;
        j = 4;
    }
    for(i; i<mas.length; i++){
        names.push(mas[i].split('=')[1]);
    }
    for(i=0; i<j; i++){
       elements[i].innerHTML = decodeURIComponent(names[i]);
    }
}

// функции валидации
// функция валидации форм на первом шаге
function validate_first(form){
    if(!validate_text(form.name.value, 'Имя')) {
        tip(document.getElementById('name'));
        return false;
    }
    if(!validate_text(form.surname.value, 'Фамилия')) {
        tip(document.getElementById('surname'));
        return false;
    }
    if(!validate_text(form.patronymic.value, 'Очество')) {
        tip(document.getElementById('patronymic'));
        return false;
    }
    if(!validate_birthday(form.birthday.value)) {
        tip(document.getElementById('datepicker'));
        return false;
    }
    if(document.getElementById('sex').disabled || document.getElementById('sex').isDisabled){
        set_cookie('Пол', form.sex.value);
    }else{
        if(!validate_text(check('sex'), 'Пол')) {
            tip(document.getElementsByName('sex'));
            return false;
        }
    }
    if(!validate_text(form.country.value, 'Страна проживания')) {
        tip(document.getElementById('country'));
        return false;
    }
    if(!validate_address(form.address.value)) {
        tip(document.getElementById('address'));
        return false;
    }
    if(!validate_text(form.mother.value, 'Девичья фамилия матери')) {
        tip(document.getElementById('mother'));
        return false;
    }
    if(!validate_text(form.code.value, 'Кодовое слово в вашем банке')) {
        tip(document.getElementById('code'));
        return false;
    }
    if(!validate_text(form.fromwho.value, 'Как вы узнали о нашем сайте')) {
        tip(document.getElementById('fromwho'));
        return false;
    }
    if(!validate_email(form.email.value)) {
        tip(document.getElementById('email'));
        return false;
    }
    if(gender == men || check('sex') == men || decodeURIComponent(get_cookie('Пол')) == men){
        if(!validate_tel(form.tel.value)) {
            tip(document.getElementById('tel'));
            return false;
        }
        if(!validate_text(form.team.value, 'За какую футбольную команду болеешь')) {
            tip(document.getElementById('team'));
            return false;
        }
    }else{
        if(!validate_tel(form.tel2.value)) {
            tip(document.getElementById('tel2'));
            return false;
        }
    }
    if(!validate_text(form.sk.value, 'Сковороду какой фирмы предпочитаешь')) {
        if(document.getElementById('sk').disabled || document.getElementById('sk').isDisabled){
            set_cookie('Сковороду какой фирмы предпочитаешь', 'Tefal');
        }else{
            tip(document.getElementById('sk'));
            return false;
        }
    }
    if(document.getElementById('edit_hide_div')){
        if(validate_second(form)){
            write_data_in_file();
            location.reload();
        }else{
            return false
        }
    }
    return true;
}
// функция валидации форм на втором шаге
function validate_second(form){
    if(!validate_card_number(form.card_number.value)) {
        tip(document.getElementById('card_number'));
        return false;
    }
    if(!validate_to_date(form.to_date.value)) {
        tip(document.getElementById('to_date'));
        return false;
    }
    if(!validate_cvc(form.cvc.value)) {
        tip(document.getElementById('cvc'));
        return false;
    }
    if(!validate_text(check('type_card'), 'Тип карты')) {
        tip(document.getElementById('type_card'));
        return false;
    }
    return true;
}
function validate_text(form, name){
    var reg = /^[A-Za-zА-Яа-яёЁ ]+$/;
    if(form == ''){
        return false;
    }else if(!(reg.test(form))) {
        return false;
    }
    set_cookie(name, form);
    return true;
}
function validate_birthday(form) {
    var reg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
    if(form == ''){
        return false;
    }else if(!(reg.test(form))) {
        return false;
    }
    set_cookie('День Рождения', form);
    return true;
}
function validate_address(form) {
    var reg = /^[а-яА-ЯёЁa-zA-Z0-9 ,/-]+$/;
    if(form == '') {
        return false;
    }else if(!(reg.test(form))) {
        return false;
    }
    set_cookie('Адрес', form);
    return true;
}
function validate_email(form) {
    var reg = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
    if(form == ''){
        return false;
    }else if(!(reg.test(form))){
        return false;
    }
    set_cookie('Email', form);
    return true;
}
function validate_tel(form) {
    var reg = /^\+\d{3} \d{2} \d{3}-\d{2}-\d{2}$/;
    if(form == ''){
        return false;
    }else if(!(reg.test(form))) {
        return false;
    }else if(!inter(form)){
        return false;
    }
    set_cookie('Телефон', form);
    return true;
}
function validate_card_number(form){
    var reg = /^\d{4} \d{4} \d{4} \d{4}$/;
    if(form == ''){
        return false;
    }else if(!(reg.test(form))) {
        return false;
    }
    set_cookie('Номер карты', form);
    return true;
}
function validate_cvc(form){
    var reg = /^\d{3}$/;
    if(form == ''){
        return false;
    }else if(!(reg.test(form))) {
        return false;
    }
    set_cookie('CVC', form);
    return true;
}
function validate_to_date(form) {
    var reg = /(0[1-9]|[12][0-9]|3[01])[-/](0[1-9]|1[012])/;
    if(form == ''){
        return false;
    }else if(!(reg.test(form))) {
        return false;
    }
    set_cookie('Срок действия', form);
    return true;
}
// конец валидации

function set_cookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updated_cookie = name + "=" + value;

    for (var propName in options) {
        updated_cookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updated_cookie += "=" + propValue;
        }
    }

    document.cookie = updated_cookie;
}
function get_cookie(name) {
    var matches = document.cookie.split('; ');
    for(var i=0; i<matches.length; i++){
        var single = matches[i].split('=');
        if(single[0] == name){
            return single[1];
        }
    }
    return undefined;
}
function delete_cookie(name) {
    set_cookie(name, "", {
        expires: -1
    })
}
function get_coockie_array(){
    var mas = document.cookie.split(';'), keys = [], values = [];
    for(var i=0; i<mas.length; i++){
        keys.push(decodeURIComponent(mas[i].split('=')[0]));
        values.push(decodeURIComponent(mas[i].split('=')[1]));
    }
    return [keys, values];
}

// функция всплывающих подсказок, если не введены или некоррестно введены данные
function tip(element){
    if(!document.getElementById('error_div')){
        var div = document.createElement('div');
        div.setAttribute('id', 'error_div');
        div.innerHTML = 'Заполните, пожалуйста, это поле';
        element.parentNode.insertBefore(div, element.previousSibling);
        div.style.position = 'absolute';
        div.style.color = 'white';
        div.style.top = parseFloat(element.offsetTop)-45+'px';
        if(browser() == 'Internet Explorer') div.style.left = parseFloat(element.offsetLeft)+'px';
        element.style.borderColor = 'red';
        // Обработчик события наведения мыши:
        function del_div() {
            div.parentNode.removeChild(div);
            div = null;
            if(browser() == 'Internet Explorer'){
                element.style.borderColor = '#bfbfbf';
                element.style.borderBottom = '1px solid lightgrey';
                element.style.borderRight = '1px solid lightgrey';
            }else{
                element.style.border = '';
            }
            element.onclick = '';
        }
        // Назначаем обработчики событий:
        element.onclick = del_div;
    }
}
// функция отображения дополнительных данных
function extra_tip(element){
    element = document.getElementById(element);
    var div = document.getElementById('extra_div');
    div.style.display = 'block';
    element.parentNode.insertBefore(div, element.nextSibling);
    show_extra_data();
    div.style.top = parseFloat(element.offsetTop)+40+'px';
    div.style.left = parseFloat(element.offsetLeft)+div.clientWidth/2+'px';
    // Обработчик события наведения мыши:
    function hide_div() {
        div.onclick = '';
        div.style.display = 'none';
    }
    div.onclick = hide_div;
}
// функция отображения дополнительных данных
function show_extra_data(){
    var table = document.getElementById('extra_table');
    var num_rows = table.rows.length;
    var i;
    // удаляем строки в таблице если ее уже открывали
    if(num_rows > 0){
        for(i=num_rows-1; i>=0; i--){
            table.deleteRow(i);
        }
    }
    var mas = document.cookie.split(';'), keys = [], values = [];
    for(i=6; i<mas.length-4; i++){
        keys.push(mas[i].split('=')[0]);
        values.push(mas[i].split('=')[1]);
    }
    var cell, row;
    for(i=0; i<mas.length-10; i++){
        row = table.insertRow(-1);
        cell = row.insertCell(-1);
        cell.innerHTML =
            '<span class="extra_title">'+decodeURIComponent(keys[i])+'</span><br>' +
            '<span class="extra_content">'+decodeURIComponent(values[i])+'</span>';
        if(keys[i] == 'Сковороду какой фирмы предпочитаешь') break;
    }
}
function show_card_data(element){
    var obj = document.getElementById(element);
    var l = ['div_card_data', 'div_own_data'];
    for(var i=0; i<l.length; i++){
        element = document.getElementById(l[i]);
        if(obj == element && element.style.display != 'block'){
            element.style.display = 'block';
        }else{
            element.style.display = 'none';
        }
    }
    show_data('card_data');
    // удаляем классы
    var block = document.getElementById('third_step');
    var labels = block.getElementsByTagName('label');
    for(i=0; i<labels.length; i++){
        if(browser() == 'Internet Explorer'){
            if(labels[i].className == 'green_doted'){
                labels[i].className = 'del_green_doted';
            }else if(labels[i].className == 'del_green_doted'){
                labels[i].className = 'green_doted';
            }
        }else{
            if(labels[i].classList.contains('green_doted')){
                labels[i].classList.remove('green_doted');
                labels[i].classList.add('del_green_doted');
            }else if(labels[i].classList.contains('del_green_doted')){
                labels[i].classList.remove('del_green_doted');
                labels[i].classList.add('green_doted');
            }
        }
    }
}
// окно с вопросом загружаем или сохраняем
function modal_window(element){
    var container = document.getElementById('container');
    var block = document.createElement('div');
    block.setAttribute('id', 'blocking');
    block.style.opacity = 0.5;
    block.style.backgroundColor = 'grey';
    block.style.filter = 'alpha(opacity=' + 50 + ')';
    block.style.position = 'absolute';
    block.style.width = 100+'%';
    block.style.height = 100+'%';
    block.style.left = '0px';
    block.style.top = '0px';
    var obj = document.getElementById(element);
    obj.style.display = 'block';
    container.parentNode.insertBefore(obj, container.nextSibling);
    obj.style.left = screen.width/2-obj.clientWidth/2+'px';
    obj.style.top = screen.height/2-obj.clientHeight+'px';
    container.parentNode.insertBefore(block, container.nextSibling);
}
// записываем данные в csv файл
// для этих целей применяю сторонний файл file_sever.js предоставляюший метод saveAs
function write_data_in_file(div){
    div = div || false;
    var keys = get_coockie_array()[0];
    var values = get_coockie_array()[1];
    var my_csv = keys.join(';')+'\n'+values.join(';');
    //window.open('data:text/csv;charset=windows-1252,%EF%BB%BF'+encodeURI(my_csv));
    var blob = new Blob([my_csv], {type: "text/csv;charset=utf-8"});
    saveAs(blob, "people_list.csv");
    if(div){
        close_modal_window(div, keys, values);
    }
}
// закрываем модальное окно
function close_modal_window(div, keys, values){
    var sign = '', params= '';
    keys = keys || get_coockie_array()[0];
    values = values || get_coockie_array()[1];
    for(var i=0; i<keys.length; i++){
        if(i != 0){
            sign = '&';
        }
        params += sign+my_trim(keys[i]+'='+values[i]);
    }
    console.log('параметры которые передаются на сервер\n'+params);
    // далее просто закоментирован текст, что бы не отправлять запрос на неизвестный url
    //var xhr = getXmlHttp();
    //xhr.open('GET', 'url?' + params, true);
    //xhr.send();
    //
    //if (xhr.status != 200) {
    //    alert(xhr.status + ': ' + xhr.statusText);
    //}else {
    //    alert(xhr.responseText);
    //}
    var block = document.getElementById('blocking');
    block.parentNode.removeChild(block);
    block = null;
    div = document.getElementById(div);
    div.parentNode.removeChild(div);
    div = null;
    var mas = document.cookie.split(';');
    for(i=0; i<mas.length; i++){
        delete_cookie(mas[i].split('=')[0]);
    }
    location.href = '/template/index.html';
}
// получаем объект запроса
function getXmlHttp(){
    try {
        return new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            return new ActiveXObject("Microsoft.XMLHTTP");
        } catch (ee) {
        }
    }
    if (typeof XMLHttpRequest!='undefined') {
        return new XMLHttpRequest();
    }
}
// удаляем начальные пробелы
function my_trim(str){
    return str.replace(/^\s+/, '');
}
// преобразование csv файла
function csv_to_array(str_data){
    var str = str_data.split('\n');
    var keys = str[0].split(';');
    var values = str[1].split(';');
    return [keys, values];
}
// выбор действия на стартовой странице
function next() {
    var val = check('how');
    if(val == 'Персональная') {
        location.href = '/template/reg.html';
    }else{
        display_data();
    }
}
// проверка радиокнопок
function check(element){
    var inp = document.getElementsByName(element);
    for (var i = 0; i < inp.length; i++) {
        if (inp[i].type == "radio" && inp[i].checked) {
            return inp[i].value;
        }
    }
}
// загружаем выбранные файлы
function read_file(event){
    var files = event.target.files;
    for(var i=0, f; f = files[i]; i++){
        var reader = new FileReader();
        reader.onload = (function(the_file){
            return function(e){
                var text = e.target.result;
                loaded_files.push([the_file.name, text]); // в 1 элементе содержится инфо о имени файла, во 2 данные
                info_file();
            };
        })(f);
        reader.onerror = function() {
            console.error("Файл не может быть прочитан!");
        };
        reader.readAsText(f);
    }
}
// информация о загруженных файлах справа от кнопки загрузить список
function info_file(){
    var info = document.getElementById('what_files');
    var number = 'а';
    if(loaded_files.length > 1){
        if(loaded_files.length > 4){
            number = 'ов';
        }
        info.innerHTML = ' | Загружено '+loaded_files.length+' файл'+number;
    }else{
        info.innerHTML = ' | '+loaded_files[0][0];
    }
}
// функция отображения сводной таблицы на стартовой странице
function display_data(){
    var div = document.getElementById('hide-design');
    div.style.display = 'block';
    var table = document.getElementById('design_table');
    for(var j=0; j<loaded_files.length; j++){
        table.insertRow(j+1);
        var data = csv_to_array(loaded_files[j][1]);
        for(var i=0; i<6; i++){
            table.rows[j+1].insertCell(i);
            table.rows[j+1].cells[i].innerHTML = data[1][i];
        }
        table.rows[j+1].insertCell(i);
        table.rows[j+1].cells[i].innerHTML = '<label class="green_doted" id='+j+' onclick="edit_table(this.id)">Правка</label>';
    }
}
function edit_table(j){
    var div = document.getElementById('edit_hide_div');
    div.style.display = 'block';
    var table = document.getElementById('main_table');
    var inputs = table.getElementsByTagName('input');
    var data = csv_to_array(loaded_files[j][1]);
    if(data[1][4] == 'Женский'){
        var td = document.getElementById('change_gender');
        td.innerHTML = 'Номер телефона твоего парня';
        var num_rows = table.rows.length;
        table.deleteRow(num_rows - 6);
        var tel = document.getElementById('tel');
        tel.setAttribute('id', 'tel2');
    }
    for(var i=0; i<inputs.length; i++){
        inputs[i].value = data[1][i];
        if(i==4){
            inputs[i].disabled = true;
        }
    }
}
// функции отправки сообщения
function send_message(who) {
    var block = document.getElementById('hide_block_for_input');
    var div = document.createElement('div');
    var color, message;
    if(who == 'robot'){
        color = '#f5f5f5';
        message = 'Здравствуйте. Пишите мне если у Вас возникли вопросы по работе сайта.';
        var div2 = document.createElement('div');
        block.appendChild(div2);
        if(browser() != 'Internet Explorer'){
            div2.classList.add('circle');
        }else{
            div2.className += ' circle';
        }
    }else{
        color = '#c8f9f0';
        message = document.getElementById('message_textarea').value;
    }
    if(message != ''){
        div.innerHTML = message;
        block.appendChild(div);
        if(browser() != 'Internet Explorer'){
            div.classList.add('message');
        }else{
            div.className += ' message';
        }
        div.style.backgroundColor = color;
        detect_border();
    }

}
// функция добавления overflow-y
function detect_border(){
    var block = document.getElementById('hide_block_for_input');
    var parent = document.getElementById('message_block');
    if(block.clientHeight >= parent.clientHeight){
        block.style.overflowY = 'auto';
        block.style.height = parent.clientHeight+'px';
    }else{
        block.style.overflowY = '';
        block.style.height = '';
    }
}
// закрываем окно с таблицей загруженных файлов
function close(){
    document.getElementById('hide-design').style.display = 'none';
    var table = document.getElementById('design_table');
    var num_rows = table.rows.length;
    for(var i=num_rows; i>1; i--){
        table.deleteRow(i-1);
    }
}
// открываем окно загрузки файлов
function show_file_input() {
    var file_input = document.getElementById("files");
    file_input.click();
}
// определяем браузер
function browser(){
    var ua = navigator.userAgent;

    if (ua.search(/MSIE/) > 0) return 'Internet Explorer';
    if (ua.search(/Firefox/) > 0) return 'Firefox';
    if (ua.search(/Opera/) > 0) return 'Opera';
    if (ua.search(/Chrome/) > 0) return 'Google Chrome';
    if (ua.search(/Safari/) > 0) return 'Safari';
    if (ua.search(/Konqueror/) > 0) return 'Konqueror';
    if (ua.search(/Iceweasel/) > 0) return 'Debian Iceweasel';
    if (ua.search(/SeaMonkey/) > 0) return 'SeaMonkey';

    // Браузеров очень много, все вписывать смысле нет, Gecko почти везде встречается
    if (ua.search(/Gecko/) > 0) return 'Gecko';

    // а может это вообще поисковый робот
    return 'Search Bot';
}